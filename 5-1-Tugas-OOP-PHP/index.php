<?php
trait  Hewan
{
    public $nama,
        $jenisHEwan,
        $darah = 50,
        $jumlahKaki,
        $keahlian;

    public function  atraksi($nama)
    {
        return $nama . "sedang " .$this->keahlian;
    }
}

trait  Fight
{

    use Hewan;
    public $attackPower,
        $defencePower;


    public function serang($sasaran)
    {
        $namaSasaran = $sasaran->nama;

        echo $this->nama." sedang menyerang ".$namaSasaran;
        echo "<br>";
        $this->diserang($sasaran);
    }

    public  function diserang($sasaran)
    {

        echo $sasaran->nama." sedang diserang ".$this->nama;
        echo "<br>";
        $darahSekarang = $sasaran->darah;
        $darahSasaran =  $darahSekarang - ($this->attackPower / $sasaran->defencePower);
        $sasaran->darah = $darahSasaran;

        
    }
}




class Elang
{
    use Hewan, Fight;
    public function __construct($nama)
    {
        $this->jenisHEwan = "Elang";
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function atraksi()
    {
        return $this->nama . "sedang " . $this->keahlian."<br>";
    }

    public function getInfoHewan(){
        echo "==========================================<br>";
        echo "Jenis hewan :".$this->jenisHEwan."<br>";
        echo "Nama hewan :".$this->nama."<br>";
        echo "Jumlah kaki :".$this->jumlahKaki."<br>";
        echo "Keahlian :".$this->keahlian."<br>";
        echo "Attack power :".$this->attackPower."<br>";
        echo "Defence power :".$this->defencePower."<br>";
        echo "Darah power :".$this->darah."<br>";
        echo "==========================================<br>";

    }
}


class Harimau
{
    use Hewan, Fight;
    public function __construct($nama)
    {
        $this->jenisHEwan = "Harimau";
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian."<br>";
    }

    public function getInfoHewan(){
        echo "==========================================<br>";
        echo "Jenis hewan :".$this->jenisHEwan."<br>";
        echo "Nama hewan :".$this->nama."<br>";
        echo "Jumlah kaki :".$this->jumlahKaki."<br>";
        echo "Keahlian :".$this->keahlian."<br>";
        echo "Attack power :".$this->attackPower."<br>";
        echo "Defence power :".$this->defencePower."<br>";
        echo "Darah power :".$this->darah."<br>";
        echo "==========================================<br>";

    }
}


// ========instansiasi======================================================================================
$harimau_1 = new Harimau('Anak harimau');
$elang_1 = new Elang('Anak elang');
// =========================================================================================================

$harimau_1->getInfoHewan();
$elang_1->getInfoHewan();

echo "<br>";
echo "<br>";
echo "<br>";

echo $harimau_1->atraksi();
echo $elang_1->atraksi();

echo "<br>";
echo "<br>";
echo "<br>";

echo $harimau_1->serang($elang_1);

echo "<br>";
echo "<br>";
echo "<br>";

$elang_1->getInfoHewan();

