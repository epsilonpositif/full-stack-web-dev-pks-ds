<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('texst');
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Users::where('email', $request->email)->first();

        $user->update([
            'password' => Hash::make($request->password)
        ]);
       


        return response()->json([
            'success' => 'true',
            'message' => 'Password telah diubah',
            'data' => [
                'user' => $user,
            ]
        ]);
    }
}
