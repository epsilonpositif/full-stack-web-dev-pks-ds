<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Events\RegisterSuccess;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //set validation
        // dd('teeees');
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = Users::create($request->all());

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(15);

        $otp_code = OtpCode::create([
            'otp'           => $otp,
            'valid_until'   => $validUntil,
            'user_id'       => $user->id,
        ]);

        event(new RegisterSuccess($otp_code));




        return response()->json([
            'success' => 'true',
            'message' => 'user berhasil dibuat silakan verifikasi',
            'data' => [
                'user' => $user,
                'otpcode' => $otp_code
            ]
        ]);
    }
}
