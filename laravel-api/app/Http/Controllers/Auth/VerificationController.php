<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Users;
use App\OtpCode;
use Illuminate\Support\Carbon;


class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
         //set validation
        // dd('teeees');
        // dd('tex');
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otpCode = OtpCode::where('otp', $request->otp)->first();
        

        if (!$otpCode) {
            return response()->json([
                'success' => 'false',
                'message' => 'OTP Code tidak ditemukan',
               
            ], 404);
        }



        if ($otpCode->valid_until < Carbon::now()) {
            return response()->json([
                'success' => 'false',
                'message' => 'OTP Code sudah kadaluarsa',
               
            ], 404);
        }


        $user = $otpCode->users;

        $user->update([
            'email_verified_at' => Carbon::now()
         
        ]);
        
        $otpCode->delete();
        return response()->json([
            'success' => 'true',
            'message' => 'Selamat akun anda terverifikasi',
            'data' => $user
        ]);

    }
}
