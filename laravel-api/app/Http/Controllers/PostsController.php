<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Data berhasil ditampilkan',
            'data' => $posts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();
        $validator = Validator::make($requestAll, [
            'judul'  => 'required',
            'deskripsi' => 'required',
        ]);



        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Posts::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi
        ]);

        /**
         * Alternatif :
         * $post = Post::create($requestAll);
         */

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil ditambahkan',
                'data' => $post
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data gagal ditambahkan'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $post = Posts::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Show Data Post',
            'data'    => $post 
        ], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $post)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'judul'   => 'required',
            'deskripsi' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Posts::findOrFail($post->id);

        if ($post) {

            //update post
            $post->update([
                'judul'     => $request->judul,
                'deskripsi'   => $request->deskripsi
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $post
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $post = Posts::findOrfail($id);

        if ($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
