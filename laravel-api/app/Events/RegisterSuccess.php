<?php

namespace App\Events;

use App\OtpCode;
use App\Users;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RegisterSuccess
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $otp;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpcode)
    {
        $this->otp = $otpcode;
        // dd($this->otp);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
