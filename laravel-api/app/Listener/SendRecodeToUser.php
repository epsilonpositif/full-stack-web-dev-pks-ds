<?php

namespace App\Listener;

use App\Events\GenerateSuccess;
use App\Mail\GenerateSuccessMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;



class SendRecodeToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateSuccess  $event
     * @return void
     */
    public function handle(GenerateSuccess $event)
    {
        Mail::to($event->otp->users->email)->send(new GenerateSuccessMail($event->otp));
    }
}
