<?php

namespace App\Listener;

use App\Events\RegisterSuccess;
use App\Mail\RegisterSuccessMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterSuccess  $event
     * @return void
     */
    public function handle(RegisterSuccess $event)
    {
        Mail::to($event->otp->users->email)->send(new RegisterSuccessMail($event->otp));
    }
}
