<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('posts', 'PostsController');
Route::apiResource('comments', 'CommentsController');
Route::apiResource('roles', 'RolesController');

Route::group(['prefix' => 'auth', 'namespace' => 'auth'], function () {
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});
// Route::post('auth/register','Auth\RegisterController')->name('auth.register');
// Route::post('auth/regenerate-otp-code', 'Auth\RegenerateOtpCodeController@regenerate')->name('auth.regenerate');
